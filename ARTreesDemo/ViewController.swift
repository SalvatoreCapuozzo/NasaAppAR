//
//  ViewController.swift
//  ARTreesDemo
//
//  Created by Jameson Quave on 7/6/17.
//  Copyright © 2017 Jameson Quave. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    @IBOutlet var scnCollectionView: UICollectionView!

    //var grids = [Grid]()
    var array: [String] = ["Melo.dae","Ciliegio.dae","Pino.dae","Pero.dae", "Cipresso.dae", "Fico.dae", "Tiglio.dae"]
    var treeNode: SCNNode?
    var alreadySet: Bool = false
    
    var imArray: [UIImage] = []
    var tdArray: [ThreeDView] = []
    var selectedItem: Int = 0
    var defaultSize: CGFloat = 0.0
    var increaseDelta: CGFloat = 50.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
        
        //let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapped))
        
        // Create a new scene
        let scene = SCNScene(named: "art.scnassets/"+array[selectedItem])!
        self.treeNode = scene.rootNode.childNode(withName: "Tree_lp_11", recursively: true)
        self.treeNode?.position = SCNVector3(0, 0, -1)
        
        self.treeNode?.isHidden = true
        
        // Set the scene to the view
        sceneView.scene = scene
        
        for i in 0..<array.count {
            let tdView = ThreeDView()
            var name = array[i]
            name.removeLast(4)
            if let scnAssetPath = Bundle.main.path(forResource: "art.scnassets/"+name, ofType: "dae") {
                try! tdView.loadWithUSDZ(arUrl: URL(fileURLWithPath:scnAssetPath),
                                            cameraPosition: SCNVector3(x: 0, y: 50, z: 150),
                                            cameraXRotation: 0,
                                            pivotPosition: SCNVector3(x: 0, y: 0, z: 0))
            }
            let renderer = SCNRenderer(device: MTLCreateSystemDefaultDevice(), options: nil)
            renderer.scene = tdView.scene
            let renderTime = TimeInterval(0)

            // Output size
            let size = CGSize(width: 300, height: 150)

            // Render the image
            let image = renderer.snapshot(atTime: renderTime, with: size,
                            antialiasingMode: SCNAntialiasingMode.multisampling4X)
            imArray.append(image)
        }

        scnCollectionView.backgroundView = UIView()
        scnCollectionView.backgroundColor = .clear
        scnCollectionView.dataSource = self
        scnCollectionView.delegate = self
        scnCollectionView.contentInset = UIEdgeInsets(top: 0,
                                                   left: 0,
                                                   bottom: 0,
                                                   right: scnCollectionView.frame.size.width - defaultSize)
    }
    
    var treeTimer = Timer()
    var currentTouch: UITouch = UITouch()
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        currentTouch = touch
        treeTimer.invalidate()
        addTree()
        treeTimer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(addTree), userInfo: nil, repeats: true)
        
    }
    
    @objc func addTree() {
        let results = sceneView.hitTest(currentTouch.location(in: sceneView), types: [ARHitTestResult.ResultType.featurePoint])
        guard let hitFeature = results.last else { return }
        let hitTransform = SCNMatrix4(hitFeature.worldTransform)
        let hitPosition = SCNVector3Make(hitTransform.m41,
                                         hitTransform.m42,
                                         hitTransform.m43)
        //let treeClone = treeNode!.clone()
        let treeClone = SCNScene(named: "art.scnassets/"+array[selectedItem])!
        treeClone.rootNode.position = hitPosition
        treeClone.rootNode.scale = SCNVector3(0.005, 0.005, 0.005)
        sceneView.scene.rootNode.addChildNode(treeClone.rootNode)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        /*
        if !alreadySet {
            guard let touch = touches.first else { return }
            let results = sceneView.hitTest(touch.location(in: sceneView), types: [ARHitTestResult.ResultType.featurePoint])
            guard let hitFeature = results.last else { return }
            let hitTransform = SCNMatrix4(hitFeature.worldTransform)
            let hitPosition = SCNVector3Make(hitTransform.m41,
                                             hitTransform.m42,
                                             hitTransform.m43)
            //let treeClone = treeNode!.clone()
            let treeClone = SCNScene(named: "art.scnassets/"+array[selectedItem])!
            treeClone.rootNode.position = hitPosition
            treeClone.rootNode.scale = SCNVector3(0.05, 0.05, 0.05)
            sceneView.scene.rootNode.addChildNode(treeClone.rootNode)
            alreadySet = true
        } else {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                self.alreadySet = false
            }
        }*/
        guard let touch = touches.first else { return }
        currentTouch = touch
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        treeTimer.invalidate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
      
        let configuration = ARWorldTrackingConfiguration()
        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    /*
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        
        let grid = Grid(anchor: anchor as! ARPlaneAnchor)
        self.grids.append(grid)
        node.addChildNode(grid)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        
        let grid = self.grids.filter { grid in
            return grid.anchor.identifier == anchor.identifier
            }.first
        
        guard let foundGrid = grid else {
            return
        }
        
        foundGrid.update(anchor: anchor as! ARPlaneAnchor)
    }*/
    /*
    @objc func tapped(gesture: UITapGestureRecognizer) {
        // Get exact position where touch happened on screen of iPhone (2D coordinate)
        let touchPosition = gesture.location(in: sceneView)
        
        let hitTestResult = sceneView.hitTest(touchPosition, types: .existingPlaneUsingExtent)
        
        if !hitTestResult.isEmpty {
            
            guard let hitResult = hitTestResult.first else {
                return
            }
            let scene = SCNScene(named: array[selectedItem])!
            scene.rootNode.position = SCNVector3(hitResult.worldTransform.columns.3.x,hitResult.worldTransform.columns.3.y, hitResult.worldTransform.columns.3.z)
            self.sceneView.scene.rootNode.addChildNode(scene.rootNode)
        }
    }
*/
    // MARK: - ARSCNViewDelegate
    
/*
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
     
        return node
    }
*/
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}

extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "carouselCell", for: indexPath) as? ARCarouselCollectionViewCell else {
            fatalError()
        }
        //let image = imageArray[indexPath.row]
        //cell.imageView.image = image
        cell.threeDView.isRotating = false
        
        cell.imageView.image = imArray[indexPath.row]
        
        //cell.threeDView = tdArray[indexPath.row]
        
        //changeTexture(index: 0)
        scnCollectionView.dataSource = self
        scnCollectionView.delegate = self
        scnCollectionView.contentInset = UIEdgeInsets(top: 0,
                                                   left: 0,
                                                   bottom: 0,
                                                   right: scnCollectionView.frame.size.width - cell.threeDView.frame.size.width)
        var name = array[indexPath.row]
        name.removeLast(4)
        cell.label.text = name
        let size = cell.imageView.frame.size.width
        defaultSize = size
        //cell.imageView.frame.size.width = defaultSize
        //cell.imageView.frame.size.height = defaultSize
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? ARCarouselCollectionViewCell {
            if cell.imageView.frame.size.width == defaultSize {
                cell.imageView.frame.origin.x -= increaseDelta/2
                cell.imageView.frame.origin.y -= increaseDelta/2
            }
            cell.imageView.frame.size.width = defaultSize + increaseDelta
            cell.imageView.frame.size.height = defaultSize + increaseDelta
            
            selectedItem = indexPath.row
        }
        //cell!.contentView.backgroundColor = .blue
        
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? ARCarouselCollectionViewCell {
            if cell.imageView.frame.size.width == defaultSize + increaseDelta {
                cell.imageView.frame.origin.x += increaseDelta/2
                cell.imageView.frame.origin.y += increaseDelta/2
            }
            cell.imageView.frame.size.width = defaultSize
            cell.imageView.frame.size.height = defaultSize
        }
        //cell!.contentView.backgroundColor = .clear
        
        
        
    }
}

extension ViewController: UICollectionViewDelegate, UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        /*
        if let index = scnCollectionView.indexPathForItem(at: view.convert(modelView.center, to: scnCollectionView))?.item {
            //changeTexture(index: index)
        }*/
    }
}


