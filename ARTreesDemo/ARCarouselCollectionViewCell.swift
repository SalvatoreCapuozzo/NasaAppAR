//
//  ARCarouselCollectionViewCell.swift
//  ARTreesDemo
//
//  Created by Salvatore Capuozzo on 19/10/2019.
//  Copyright © 2019 Jameson Quave. All rights reserved.
//

import UIKit
import SceneKit

class ARCarouselCollectionViewCell: UICollectionViewCell {
    @IBOutlet var threeDView: ThreeDView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var label: UILabel!
}
